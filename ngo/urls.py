"""ngo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from ngoman import views as main

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^$', main.dashboard),
    url(r'^people/$', main.peopleview),
    url(r'^prog/$', main.programs),
    url(r'^ajax/$', main.ajaxproc),
    url(r'^ajaxrpt/$', main.ajaxprocrpt),
    url(r'^ngoadmin/$', main.ngoadmin),
    url(r'^reports/$', main.reports),
    url(r'^addperson/(?P<person>\d+)?/?$', main.addperson),
    url(r'^viewperson/(?P<person>\d+)?/?$', main.viewperson),
    url(r'^adduser/(?P<user>\d+)?/?$', main.adduser),
    url(r'^docs/(?P<person>\d+)/$', main.documents),
    url(r'^attendance/(?P<prog>\d+)?/?$', main.attendanceops),
    url(r'^manatt/(?P<person>\d+)?/?$', main.manatt),
    url(r'^search/', include('haystack.urls')),
    url(r'^changepass/(?P<user>\d+)?/?$', main.changepass),
    url(r'^getattendance/(?P<prog>\d+)?/?$', main.getattendance),
    url(r'^viewreports/?$', main.viewreports),
]
