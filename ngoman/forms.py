# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from ngoman.models import people, document, orgdoc, locality
from django.contrib.auth.models import User


class personForm(forms.ModelForm):
    class Meta:
        model = people
        fields = ['fname', 'sname', 'gender', 'docscomplete', 'country', 'maritialstatus', 'regdate', 'education', 'children', 'phonenum', 'idnum', 'address', 'locality', 'town', 'birthdate', 'programmes', 'notes', 'olddata', 'arrivaldate', 'speaktr', 'archived']
        labels = {
            "fname": "Ad",
            "sname": "Soyad",
            "phonenum": "Telefon",
            "idnum": "Kimlik No",
            "gender": "Cinsiyet",
            "docscomplete": u"Belgeleri Tamam mı?",
            "country": "Ülke",
            "maritialstatus": "Medeni Hali",
            "education": "Tahsil",
            "children": u"Çocukları",
            "address": "Adres",
            "locality": "Mahalle",
            "town": "İlçe",
            "birthdate": u"Doğum Tarihi",
            "programmes": "Üye Olduğu Programlar",
            "notes": "Notlar",
            "olddata": "Eski Bilgiler",
            "arrivaldate": u"Ülkeye Varış Tarihi",
            "regdate": u"Kayıt Tarihi",
            "speaktr": u"Türkçe biliyor",
            "archived": u"Kayıt Durumu",
        }

    def __init__(self, *args, **kwargs):
        super(personForm, self).__init__(*args, **kwargs)
        self.fields['locality'].queryset = locality.objects.none()

class OrgDocsForm(forms.ModelForm):
    class Meta:
        model = orgdoc
        fields = ['filename', 'description']

class OrgDocsUpdateForm(forms.ModelForm):
    filename = forms.FileField(required=False)
    class Meta:
        model = orgdoc
        fields = ['filename', 'description']

class PersonalDocsUpdateForm(forms.ModelForm):
    filename = forms.FileField(required=False)

    class Meta:
        model = document
        fields = ['filename', 'doctype', 'description']

class PersonalDocsForm(forms.ModelForm):
    myid = forms.IntegerField(widget=forms.HiddenInput)

    class Meta:
        model = document
        fields = ['filename', 'doctype', 'description', 'myid']

class AdminUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

class UserUpdateForm(forms.ModelForm):
    #password = forms.CharField(required=False, widget=forms.PasswordInput)
    #password_confirm = forms.CharField(required=False, widget=forms.PasswordInput())

    class Meta:
        model = User
        # Add all the fields you want a user to change
        fields = ('first_name', 'last_name', 'username', 'email', 'is_superuser')

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    password_confirm = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'is_superuser', 'password')
