from django import template
from datetime import timedelta
from datetime import date

register = template.Library()

@register.simple_tag
def lastweek(format):
    lastweek = date.today() - timedelta(days=7)
    return lastweek.strftime(format)
