# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from models import people, programmes, doctype, document, log, attendance, orgdoc, locality
from forms import PersonalDocsForm, PersonalDocsUpdateForm, personForm, UserForm, UserUpdateForm
from forms import AdminUpdateForm, OrgDocsForm, OrgDocsUpdateForm
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models import Avg
from django.db.models import Q
from django.contrib.auth.forms import SetPasswordForm

from datetime import datetime
from itertools import chain
import json
import csv
import os, sys
import logging
import inspect
import StringIO
from xlsxwriter import Workbook
import syslog
from django.contrib.auth.signals import user_logged_in, user_logged_out, user_login_failed
from django.dispatch import receiver

############## Helper Methods ####################
logger = logging.getLogger(__name__)

def handle_uploaded_file(f, docname, pid):
    doc_path = settings.STATIC_ROOT + '/documents/' + str(pid) + '/'
    if not os.path.exists(doc_path):
        os.makedirs(doc_path)

    with open(doc_path + docname, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def log_event(request, event):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ipaddress = x_forwarded_for.split(',')[-1].strip()
    else:
        ipaddress = request.META.get('REMOTE_ADDR')

    mylog = log()
    mylog.user = request.user
    mylog.event = event
    mylog.ipaddr = ipaddress
    mylog.save()
######################
@receiver(user_logged_in)
def user_logged_in_callback(sender, request, user, **kwargs):
    log_event(request, 'User %s logged in' % user.username)

@receiver(user_logged_out)
def user_logged_out_callback(sender, request, user, **kwargs):
    log_event(request, 'User %s logged out' % user.username)

@login_required
def dashboard(request):
    pcountall = people.objects.all().count()
    pcountactive = people.objects.filter(archived='A').count()
    prcountall = programmes.objects.all().count()
    progs = programmes.objects.filter(active=True).order_by('pname')
    practive = progs.count()
    adults = people.adult_objects.filter(archived='A')
    children = people.child_objects.filter(archived='A')
    if adults.count() != 0:
        adultage = sum(map(lambda x: x.age(), adults)) / adults.count()
    else:
        adultage = 0
    if children.count() != 0:
        childage = sum(map(lambda x: x.age(), children)) / children.count()
        childpadult = children.count()/ adults.count()
    else:
        childage = 0
        childpadult = 0
    return render(request, 'dashboard.html', {'pcountall': pcountall, 'pcountactive': pcountactive, 'prcountall': prcountall, 'practive': practive, 'avgadultage': adultage, 'avgchildage': childage, 'progs': progs, 'childpadult': childpadult})


@login_required
def ngoadmin(request):
    if not request.user.is_superuser:
        return HttpResponseRedirect('/')

    msg = ''
    if request.method == 'POST':
        if request.POST.get('delperson', False):
            person2rm = request.POST['delperson']
            if person2rm != 0:
                myperson = people.objects.get(id=person2rm)
                tmpname = myperson.fullname()
                myperson.delete()
                msg = 'Sığınmacı başarıyla silindi!'
                log_event(request, 'Person %s deleted from the system' % (tmpname))
                del(tmpname)
        elif request.POST.get('action', False):
            if request.POST['action'] == 'orgdocupload':
                myid = request.POST.get('myid', '')
                if myid != '':
                    mydoc = orgdoc.objects.get(id=myid)
                    myform = OrgDocsUpdateForm(request.POST, request.FILES, instance=mydoc)
                else:
                    myform = OrgDocsForm(request.POST, request.FILES)
                if myform.is_valid():
                    if myid != '':
                        log_event(request, 'Document %s updated' % (mydoc.filename))
                    else:
                        log_event(request, 'New document %s uploaded' % (request.FILES['filename']))
                    myform.save()
                    return redirect('/ngoadmin/#orgdocs')

    mydoctypes = doctype.objects.all()
    orgdocf = OrgDocsForm()
    person = people.objects.all().order_by('fname','sname')
    return render(request, 'ngoadmin.html', { 'doctype': mydoctypes, 'orgdocf': orgdocf , 'person': person, 'msg': msg})

@login_required
def reports(request):
    progs = programmes.objects.filter(active=True).order_by('pname')
    return render(request, 'reports.html', {'progs': progs})

@login_required
def peopleview(request):
    return render(request, 'people.html')


@login_required
def programs(request):
    proglist = programmes.objects.all().order_by('pname')
    progs = []
    for i in proglist:
        count = people.objects.filter(programmes=i, archived='A').count()
        progs.append({'name': i.pname, 'count': count , 'active': i.active})
    return render(request, 'progs.html', { 'progs': progs })

@login_required
def documents(request, person):
    mydocs = document.objects.filter(owner=person).select_related()
    myperson = people.objects.get(id=person)
    myname = myperson.fullname()
    if request.method == 'POST':
        myid = request.POST['myid']
        if myid != '':
            mydoc = document.objects.get(id=myid)
            myform = PersonalDocsUpdateForm(request.POST, request.FILES, instance=mydoc)
        else:
            myform = PersonalDocsForm(request.POST, request.FILES)
        if myform.is_valid():
            if myid != '':
                log_event(request, 'Document %s updated' % (mydoc.filename))
            else:
                log_event(request, 'New document %s uploaded' % (request.FILES['filename']))
            myform.save()
            return redirect('/docs/'+str(person)+'/?adddocs=true')
        else:
            return render(request, 'documents.html', {'mydocs': mydocs, 'myform': myform, 'myname': myname, 'myperson': myperson, 'edit':True })
    else:
        myform = PersonalDocsForm()

    return render(request, 'documents.html', {'mydocs': mydocs, 'myform': myform, 'myname': myname, 'myperson': myperson, 'edit': False })

@login_required
def adduser(request, user=None):
    if not request.user.is_superuser:
        return redirect('/')

    edit = False
    if request.method == "POST":
        if user:
            u = User.objects.get(id=user)

            if u.username == 'admin':
                user_form = AdminUpdateForm(data=request.POST, instance=u)
            else:
                user_form = UserUpdateForm(data=request.POST, instance=u)
            edit = True
        else:
            user_form = UserForm(request.POST)

        if user_form.is_valid():
            if edit == False:
                new_user = User.objects.create_user(username=user_form.cleaned_data['username'],email=user_form.cleaned_data['email'],password=user_form.cleaned_data['password'])
                new_user.save()
                log_event(request, 'New user %s added' % (user_form.cleaned_data['username']))
            else:
                user_form.save()
                log_event(request, 'User %s updated' % (u.username))
            return HttpResponseRedirect('/ngoadmin/')
        elif user_form.data['password'] != user_form.data['password_confirm']:
                user_form.add_error('password_confirm', 'The passwords do not match')
    else:
        if not user:
            user_form = UserForm()
        else:
            u = User.objects.get(id=user)
            if u.username == 'admin':
                user_form = AdminUpdateForm(instance=u)
            else:
                user_form = UserUpdateForm(instance=u)
            edit = True

    return render(request, 'addperson.html', {'myform': user_form, 'edit': edit, 'type': 'Kullanıcı'})

@login_required
def viewperson(request, person=None):
    if not person:
        return redirect('/people/')
    p = people.objects.get(id=person)
    parents = people.objects.filter(children=p)
    return render(request, 'viewperson.html', {'person': p, 'parents': parents})

@login_required
def addperson(request, person=None):
    if not request.user.is_superuser:
        return redirect('/')

    edit = False
    if request.method == 'POST':
        if person:
            p = people.objects.get(id=person)
            form = personForm(data=request.POST, instance=p)
        else:
            form = personForm(request.POST)
        if form.is_valid():
            form.save()
            log_event(request, 'New person added')
            return redirect('/people/?adduser=true')
    else:
        if not person:
            form = personForm()
        else:
            myperson = people.objects.get(id=int(person))
            form = personForm(instance=myperson)
            edit = True

    return render(request, 'addperson.html', {'myform': form, 'edit': edit, 'type': 'Sığınmacı'})

@login_required
def manatt(request, person=None):
    myperson = people.objects.get(id=person)
    status = 'none';
    if request.method == 'POST':
        mydates = request.POST['dates'];
        myprogs = request.POST['progs'];
        myprog = programmes.objects.get(id=myprogs)
        #a, created = attendance.objects.get_or_create(programme=myprog,attdate=datetime.strptime(mydates, '%d/%m/%Y').date())
        a, created = attendance.objects.get_or_create(programme=myprog,attdate=request.POST['dates'])
        if created:
            a.people = '{"' + str(person) + '": "t"}'
        else:
            temparr = a.people
            temparr[str(person)] = 't'
            a.people = temparr
        a.save()
        status = 'created'
    return render(request, 'manatt.html', {'person': myperson, 'status': status })

@login_required
def getattendance(request, prog=None):
    myprog = programmes.objects.get(id=prog)
    mypeople = people.objects.filter(programmes=myprog).order_by('fname')
    if request.method == 'GET' and 'contrpt' in request.GET:
        contrpt = request.GET['contrpt']

    if contrpt == 'true':
        if request.GET['cont']:
            contnum = int(request.GET['cont'])
        else:
            contnum = 4
        complete_att = attendance.objects.filter(programme=myprog).order_by('-attdate')[:4]
    else:
        complete_att = attendance.objects.filter(programme=myprog).order_by('attdate')
    mydates = []
    for i in complete_att:
        thisdate = i.attdate
        if request.method == 'GET' and 'start' in request.GET and 'end' in request.GET:
            startd = datetime.strptime(request.GET['start'], '%Y-%m-%d').date()
            endd = datetime.strptime(request.GET['end'], '%Y-%m-%d').date()
            if thisdate <= endd and thisdate >= startd:
                mydates.append(thisdate.strftime("%Y-%m-%d"))
        else:
            mydates.append(thisdate.strftime("%Y-%m-%d"))

    myret = []
    for i in mypeople:
        tmplist = []

        if request.method == 'GET' and 'start' in request.GET and 'end' in request.GET:
            attlist = complete_att.filter(attdate__gte=startd,attdate__lte=endd)
        else:
            attlist = complete_att
        for j in attlist:
            #Debugging purposes
            # Django dumps these variables in its debug page
            #curdate = j.attdate
            personid = str(i.id)

            #myatt = complete_att.get(attdate=curdate)
            try:
                attdata = j.people[personid]
                try:
                    tmplist.append(str(attdata))
                except:
                    tmplist.append('f')
            except:
                tmplist.append('-')

        myret.append({
            'fullname': i.fullname(),
            'idnum': i.idnum,
            'archived': i.archived,
            'mydates': tmplist
            })
    if contrpt == 'true':
        counter = 0
        contstr = []
        while True:
            if counter == contnum:
                break
            contstr.append('f')
            counter = counter + 1

        #assert False, i
        #myret = [x for x in myret if x['mydates'] == contstr]
    return render(request, 'showattendance.html', {'mydates': mydates, 'myprog': myprog, 'myret': myret})

@login_required
def attendanceops(request, prog=None):
    ''' Prog is needed '''
    if not prog:
        return redirect('/prog/')
    myprog = programmes.objects.get(id=prog)
    mypeople = people.objects.filter(programmes=myprog).order_by('fname','sname')
    try:
        att = attendance.objects.get(programme=myprog,attdate=datetime.now())
        ''' We keep attendance data as json in the postgresql db '''
        attarr = att.people
    except:
        attarr = []
    personarr = []
    for i in mypeople:
        try:
            personarr.append([i.fullname(),attarr[str(i.id)], i.id])
        except:
            personarr.append([i.fullname(),'f', i.id])
    return render(request, 'attendance.html', {'people': personarr, 'prog': myprog})

@login_required
def changepass(request, user=None):
    ''' Regular users can only change their own passwords '''
    if not user:
        myuser = request.user
    elif not request.user.is_superuser:
        return HttpResponseRedirect('/')
    else:
        myuser = User.objects.get(id=user)

    if request.method == 'POST':
        if not request.user.is_superuser:
            return HttpResponseRedirect('/')

        form = SetPasswordForm(myuser, request.POST);
        if form.is_valid():
            form.save()
            log_event(request, 'Password for %s changed' % (myuser.get_full_name()))
            return HttpResponseRedirect('/ngoadmin/')
    else:
        form = SetPasswordForm(myuser)
    return render(request, 'addperson.html', {'myform': form, 'edit': True, 'type': 'Kullanıcı'})

# Most of the actions are done here.
@login_required
def ajaxproc(request):
    response_data = {}
    getmeta = request.META.get('CONTENT_TYPE')

    if getmeta.startswith('application/json'):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        action = body['action']
    else:
        action = request.POST['action']

    if action == 'loadlocality':
        myid = request.POST['town']
        localities = locality.objects.filter(townid=myid).order_by('lname')
        return render(request, 'locality_dropdown_options.html', {'localities': localities})

    elif action == 'peoplelist':
        if request.POST['archived'] == 'notdeactive':
            mypeople = people.objects.filter(Q(archived='A')|Q(archived='W')|Q(archived='H')).order_by('fname')
        elif request.POST['archived'] != 'T':
            mypeople = people.objects.filter(archived=request.POST['archived']).order_by('fname')
        else:
            mypeople = people.objects.all().order_by('fname')
        response_data['result'] = 0
        response_data['data'] = []
        for i in mypeople:
            tmparr = []
            for j in i.programmes.all():
                tmparr.append(j.id)
            response_data['data'].append({
                    'fname' : i.fname,
                    'sname' : i.sname,
                    'age' : i.age(),
                    'idnum': i.idnum,
                    'children': i.children.count(),
                    'id': i.id,
                    'active' : i.archived,
                    'progs': tmparr,
            })
    elif action == 'orgdoclist':
        mydocs = orgdoc.objects.all()
        response_data['data'] = []
        for i in mydocs:
            response_data['data'].append({
                'filename' :i.get_filename(),
                'desc' : i.description,
                'udate' : i.uploaddate.strftime("%Y-%m-%d %H:%M:%S"),
                'id' : i.id
                })
    elif action == 'userlist':
        users = User.objects.all()
        response_data['data'] = []
        for i in users:
            if not i.last_login:
                last_login = 'N/A'
            else:
                last_login = i.last_login.strftime("%Y-%m-%d %H:%M:%S")
            response_data['data'].append({
                'username' : i.username,
                'email' : i.email,
                'admin' : i.is_superuser,
                'lastlogin' : last_login,
                'id': i.id,
            })
    elif action == 'loglist':
        logs = log.objects.all()
        response_data['data'] = []
        for i in logs:
            response_data['data'].append({
                'user' : i.user.username,
                'event': i.event,
                'ip': i.ipaddr,
                'eventtime': i.eventtime.strftime("%Y-%m-%d %H:%M:%S"),
            })
    elif action == 'proglist':
        myprogs = programmes.objects.all().order_by('pname')
        response_data['data'] = []
        for i in myprogs:
            count = people.objects.filter(programmes=i).count()
            response_data['data'].append({
                'pname': i.pname,
                'count': count,
                'active': i.active,
                'id': i.id,
                'distprod': i.distprod
            })
    elif action == 'loglist':
        mylogs = log.objects.all()
        response_data['result'] = 0
        response_data['data'] = []
        for i in mylogs:
            response_data['data'].append({
                'user': i.user,
                'eventtime': i.eventtime,
                'ipaddr': i.ipaddr,
                'event': i.event
            })
    elif action == 'getprogpeople':
        myprog = programmes.objects.filter(id=request.POST['prog'])
        mypeople = people.objects.filter(programmes__in=myprog).order_by('fname')
        response_data['result'] = 0
        response_data['data'] = []
        complete_att = attendance.objects.filter(programme=myprog)
        response_data['data'].append({
                'fullname': 'Ad Soyad',
                'country': 'Tabiyeti',
                'birthdate': 'Doğum tarihi',
                'idnum': 'Kimlik no',
                'archived': 'Arşiv',
                'mydates': tmplist
            })
        for i in mypeople:
            mydates = complete_att.objects.filter(person__data=i.id)
            tmplist = ''
            if mydates:
                for j in mydates:
                    tmplist = tmplist + ", " + j.attdate.strftime("%Y-%m-%d")
            response_data['data'].append({
                'fullname': i.fullname(),
                'country': i.country.cname_en,
                'birthdate': i.birthdate.strftime('%Y/%m/%d'),
                'idnum': i.idnum,
                'archived': i.archived,
                'mydates': tmplist
                })
    elif action == 'getprog':
        #XXX this should return sub programmes or prog groups
        myprog = programmes.objects.get(pname=request.POST['pname'])
        response_data['result'] = 0
    elif action == 'addpeople2prog':
        ppeople = request.POST.getlist('people[]')
        pprog = request.POST['prog']
        myprog = programmes.objects.get(id=pprog)
        mypeople = people.objects.filter(id__in=ppeople)
        for i in mypeople:
            i.archived = 'A'
            i.programmes.add(myprog)
            i.save()
        response_data['result'] = 0
        response_data['msg'] = 'kisiler added successfully'
    elif action == 'addprog':
        myid = request.POST['id']
        pname = request.POST['name']
        active = request.POST['active']
        distprod = request.POST['distprod']
        if active == 'true':
            activest = True
        else:
            activest = False

        try:
            myprog = programmes.objects.get(id=myid)
            myprog.pname = pname
            myprog.distprod = distprod
            myprog.active = activest
        except:
            myprog = programmes(pname=pname,distprod=distprod,active=activest)

        try:
            myprog.save()
            response_data['result'] = 0
            response_data['msg'] = 'Program başarıyla eklendi'
            log_event(request, 'New programme %s added' % (pname))
        except:
            response_data['result'] = 1
            response_data['msg'] = 'Error adding programme'

    elif action == 'deletedoct':
        try:
            mydoct = doctype.objects.get(dtname=request.POST['item'])
            mydoct.delete()
            response_data['result'] = 0
            response_data['msg'] = 'Belge tipi başarıyla silindi'
            log_event(request, 'Document type %s deleted' % (request.POST['item']))
        except:
            response_data['result'] = 1
            response_data['msg'] = 'Belge tipi silinemedi!'
    elif action == 'delorgdoc':
        try:
            mydoc = orgdoc.objects.get(id=request.POST['id'])
            mydoc.delete()
            response_data['result'] = 0
            # XXX Might want to delete the doc itself
        except:
            response_data['result'] = 1
    elif action == 'deleteuser':
        try:
            u = User.objects.get(id=request.POST['item'])
            user = u.username
            if user != 'admin':
                u.delete()
            response_data['result'] = 0
            response_data['msg'] = 'Kullanıcı başarıyla silindi'
            log_event(request, 'User %s deleted' % (user))
        except:
            response_data['result'] = 1
            response_data['msg'] = 'Kullanıcı silinemedi!'
    elif action == 'delprog':
        try:
            myprog = programmes.objects.get(pname=request.POST['prog'])
            myprog.delete()
            response_data['result'] = 0
            response_data['msg'] = 'Program başarıyla silindi'
            log_event(request, 'Programme %s deleted' % (request.POST['prog']))
        except:
            response_data['result'] = 1
            response_data['msg'] = 'Program silinemedi!'

    elif action == 'toggleprog':
        try:
            myprog = programmes.objects.get(id=request.POST['prog'])
            if myprog.active == False:
                myprog.active = True
                mystr = 'aktive'
            else:
                myprog.active = False
                mystr = 'deaktive'
            myprog.save()
            response_data['result'] = 0
            response_data['msg'] = 'Program %s edildi' % (mystr)
            log_event(request, 'Programme %s %s sucessfully' % (myprog.pname, mystr))
        except:
            response_data['result'] = 1
            response_data['msg'] = 'Error changing programme'
    elif action == 'personarchive':
        try:
            myperson = people.objects.get(id=request.POST['id'])
            if request.POST['archive'] == '0':
                # Activate
                myperson.archived = 'A'
            else:
                # Deactive
                myperson.archived = 'D'
            myperson.save()
            response_data['result'] = 0
            log_event(request, '%s arsivlendi' % (myperson.fullname()))
        except:
            response_data['result'] = 1
            response_data['msg'] = 'Error activating/deactivating'

    elif action == 'recordatt':
        myprog = programmes.objects.get(id=body['prog'])
        att, created = attendance.objects.get_or_create(programme=myprog,attdate=datetime.now())
        att.people = body['data']
        att.save()
        response_data['result'] = 0
        response_data['msg'] = 'Yoklama Kaydedildi'
    elif action == 'deletedoc':
        try:
            mydoc = document.objects.get(id=request.POST['docid'])
            docname = mydoc.filename
            mydoc.delete()
            response_data['result'] = 0
            response_data['msg'] = 'Dokuman silindi'
            log_event(request, 'Doc %s deleted' % (docname))
        except:
            response_data['result'] = 1
            response_data['msg'] = 'Error deleting document'

    elif action == 'adddoctype':
        try:
            if request.POST['olddtname'] and request.POST['olddtname'] != '':
                mydoctype = doctype.objects.get(dtname=request.POST['olddtname'])
                mydoctype.dtname = request.POST['dtname']
                msg = 'Document type renamed from %s to %s' % (request.POST['olddtname'], request.POST['dtname'])
            else:
                mydoctype = doctype(dtname=request.POST['dtname'])
                msg = 'Document type saved successfully'
            mydoctype.save()
            response_data['result'] = 0
            response_data['msg'] = 'Document type saved successfully'
            log_event(request, msg)
        except:
            response_data['result'] = 1
            response_data['msg'] = 'Error saving Document type'
    else:
        response_data['result'] = 1
        response_data['msg'] = 'Error'

    return HttpResponse(json.dumps(response_data), content_type="application/json")

'''
TODO Merge following method with ajaxrpt. 
'''
@login_required
def viewreports(request):
    reporttype = None
    reqtype = request.POST['type']
    if reqtype == "incompletedocs":
        mypeople = people.objects.filter(archived=request.POST['deactive'],docscomplete=False).order_by('fname')
        retobject = mypeople
    elif reqtype == "agerpt":
        deactive = request.POST['deactive']
        gtlt = request.POST['agerptlimit']
        age = request.POST['age']
        if deactive != 'T':
            mypeople = people.agefilter.get_queryset(int(age), gtlt).filter(archived=deactive)
        else:
            mypeople = people.agefilter.get_queryset(int(age), gtlt)
        retobject = mypeople
    elif reqtype == "contrpt":
        myprog = programmes.objects.get(id=request.POST['prog'])
        myatt = attendance.objects.filter(programme=myprog).order_by('-attdate')[:int(request.POST['contrptval'])]
        tmpdict = dict()
        counter = 0
        for i in myatt:
            personarr = i.people
            if not personarr or personarr == '':
                continue
            for k,v in personarr.iteritems(): 
                if v == 'f':
                    try:
                        tmp2 = tmpdict[k]
                        tmpdict[k] += tmp2
                    except KeyError:
                        tmpdict[k] = 1

        # this can possibly be integrated with above loop
        tmparr2 = []
        for k,v in tmpdict.iteritems():
            if v < request.POST['contrptval']:
                continue
            curpers = people.objects.get(id=k)
            tmparr = [curpers.fullname, curpers.idnum, v]
            tmparr2.append(tmparr)
        retobject = tmparr2
        del(tmparr2)
        del(tmpdict)

    return render(request, 'viewreports.html',{'type': reqtype, 'object': retobject})

# AJAX actions for reports
@login_required
def ajaxprocrpt(request):
    output = StringIO.StringIO()
    workbook = Workbook(output)
    worksheet = workbook.add_worksheet()
    now = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    today = datetime.now().strftime("%Y-%m-%d")

    action = request.POST['action']

    if action == 'childage':
        # XXX add also children name to the report
        file_name = "people_with_children" + now
        myage = request.POST['childage']
        titlearr = ["İsim/Name", "Soyad/Last Name", "Kimlik No/ID No"]
        for col, data in enumerate(titlearr):
            worksheet.write(0, col, data)
        row = 1

        uniq_check_arr = []
        for obj in people.child_objects.all():
            if obj.age() <= int(myage):
                if request.POST['deactive'] == 'T':
                    myparent = people.objects.get(children__in=obj)
                else:
                    myparent = people.objects.get(children__in=obj,archived=request.POST['deactive'])

                if myparent.id in uniq_check_arr:
                    continue

                worksheet.write(row, 0, myparent.fname)
                worksheet.write(row, 1, myparent.sname)
                worksheet.write(row, 2, myparent.idnum)
                row += 1
                uniq_check_arr.append(myparent.id)
        workbook.close()
    elif action == 'agerpt':
        file_name = "age_report" + now
        agelimit = request.POST['personage']
        # Greater or less than?
        gtlt = request.POST['agerptlimit']
        deactive = request.POST['deactive']
        if deactive != 'T':
            mypeople = people.agefilter.get_queryset(int(agelimit), gtlt).filter(archived=request.POST['deactive'])
        else:
            mypeople = people.agefilter.get_queryset(int(agelimit), gtlt)
        worksheet.write(0, 0, 'Kisi')
        worksheet.write(0, 1, 'Kimlik no')
        worksheet.write(0, 2, 'Yas')
        row = 1
        for i in mypeople:
            worksheet.write(row, 0, i.fullname())
            worksheet.write(row, 1, i.idnum)
            worksheet.write(row, 2, i.age())
            row += 1
        workbook.close()
    elif action == 'docrpt':
        file_name = "eksik_belge_raporu" + now
        deactive = request.POST['deactive']
        if deactive != 'T':
            mypeople = people.objects.filter(docscomplete=False,archived=deactive)
        else:
            mypeople = people.objects.filter(docscomplete=False)
        mypeople.order_by('fname', 'sname')
        titlearr = ["İsim/Name", "Soyad/Last Name", "Kimlik No/ID No"]
        for col, data in enumerate(titlearr):
            worksheet.write(0, col, data)
        row = 1
        for i in mypeople:
            worksheet.write(row, 0, i.fname)
            worksheet.write(row, 1, i.sname)
            worksheet.write(row, 2, i.idnum)
            row += 1
        workbook.close()
    elif action == 'distreport':
        myprog = programmes.objects.get(id=request.POST['prog'])
        file_name = myprog.pname + "_dagitim_raporu-" + now
        mypeople = people.objects.filter(programmes=myprog).order_by('fname', 'sname')
        merge_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'left',
            'valign': 'vcenter',
            'fg_color': 'gray'})
        worksheet.merge_range('A3:D3', today + ' için ' + myprog.pname + ' Programı Dağıtım Listesi', merge_format)
        worksheet.insert_image('E1', settings.BASE_DIR + '/static/images/logowithname.png', {'y_scale': 0.6})
        titlearr = ["İsim/Name", "Soyad/Last Name", "Kimlik No/ID No", "Adres/address", "Teslim Alınan " + myprog.distprod, "İmza/Signature", "Notlar/Notes"]
        for col, data in enumerate(titlearr):
            worksheet.write(3, col, data)
        row = 4
        for p in mypeople:
            worksheet.write(row, 0, p.fname)
            worksheet.write(row, 1, p.sname)
            worksheet.write(row, 2, p.idnum)
            worksheet.write(row, 3, p.address)
            row += 1
        workbook.close()

    elif action == 'contrpt':
        file_name = "devam_raporu" + now
        myprog = programmes.objects.get(id=request.POST['prog'])
        myatt = attendance.objects.filter(programme=myprog).order_by('-attdate')[:int(request.POST['contvalue'])]
        tmpdict = dict()
        counter = 0
        for i in myatt:
            personarr = i.people
            if not personarr or personarr == '':
                continue
            for k,v in personarr.iteritems(): 
                if v == 'f':
                    try:
                        tmp2 = tmpdict[k] 
                        tmpdict[k] += tmp2
                    except KeyError:
                        tmpdict[k] = 1

        worksheet.write(0, "Kisi", "Kimlik No", "Devamsizlik")
        row = 1
        for person, att in tmpdict.iteritems():
            if att >= 4:
                curperson = people.objects.get(id=person)
                worksheet.write(row, 0, curperson.fullname())
                worksheet.write(row, 1, curperson.idnum)
                worksheet.write(row, 2, att)
                row += 1

        workbook.close()
    elif action == 'attrpt':
        file_name = "yoklama_raporu" + now
        myprog = programmes.objects.get(id=request.POST['prog'])
        deactive = request.POST['deactive']
        if deactive != 'T':
            mypeople = people.objects.filter(programmes=myprog, archived=deactive).order_by('fname', 'sname')
        else:
            mypeople = people.objects.filter(programmes=myprog).order_by('fname', 'sname')
        startd = request.POST['startd']
        endd = request.POST['endd']
        mydates = attendance.objects.filter(programme=myprog, attdate__range=(startd, endd)).values('attdate').distinct().order_by('attdate')

        datearr = ["Kisi", "Kimlik No"]
        for i in mydates:
            datearr.append(i['attdate'].strftime("%Y-%m-%d"))
        row = 0
        for col, data in enumerate(datearr):
            worksheet.write(row, col, data)
        row += 1

        #This is ugly but easier to read
        for i in mypeople:
            worksheet.write(row, 0, i.fullname())
            worksheet.write(row, 1, i.idnum)
            col = 2
            for j in mydates:
                try:
                    a = attendance.objects.get(programme=myprog,attdate=j['attdate'])
                    personarr = a.people
                    if personarr[str(i.id)] == 't':
                        worksheet.write(row,col,'Var')
                    else:
                        worksheet.write(row,col,'Yok')
                    col += 1
                except Exception, e:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    error = '%s %s' % (e ,exc_tb.tb_lineno)
                    logger.error(error)
                    worksheet.write(row,col,'Yok')
                    col += 1
            row += 1
        workbook.close()

    output.seek(0)
    response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename="+file_name+".xlsx"
    #response['Content-Disposition'] = 'inline; filename=' + file_name + '.xlsx'
    return response
