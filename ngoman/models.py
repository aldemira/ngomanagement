# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import force_bytes
from django.contrib.postgres.fields import JSONField

from django.db import models
from datetime import date
import datetime
from django.contrib.auth.models import User
import os

def get_upload_path(instance, filename):
    return "static/documents/%s/%s" % (instance.owner.id, filename)

class AgeLimitManager(models.Manager):
    def get_queryset(self, agelimit, gtlt):
        limitdate = datetime.datetime.now() - datetime.timedelta(days=agelimit*365)
        if gtlt == 0:
            return super(AgeLimitManager, self).get_queryset().filter(birthdate__lte=limitdate)
        else:
            return super(AgeLimitManager, self).get_queryset().filter(birthdate__gte=limitdate)

class AdultManager(models.Manager):
    def get_queryset(self):
        adultbdate = datetime.datetime.now() - datetime.timedelta(days=18*365)
        return super(AdultManager, self).get_queryset().filter(birthdate__lte=adultbdate)

class ChildManager(models.Manager):
    def get_queryset(self):
        childbdate = datetime.datetime.now() - datetime.timedelta(days=18*365)
        return super(ChildManager, self).get_queryset().filter(birthdate__gt=childbdate)

# Create your models here.
'''
class isttown(models.Model):
    tname = models.CharField()
'''

class country(models.Model):
    ccode = models.CharField(max_length=2, blank=True)
    cname_tr = models.CharField(max_length=255, blank=True)
    cname_en = models.CharField(max_length=255, blank=True)
    class Meta:
        ordering = ["cname_tr"]

    def __str__(self):
        return self.cname_tr.encode('utf8')

class programmes(models.Model):
    pname = models.CharField(max_length=255, help_text='Programme name', unique=True)
    distprod = models.CharField(max_length=255, help_text='Distributed good', blank=True)
    active = models.BooleanField(default=True)

    def get_absolute_url(self):
        return u"/prog/?q=%s" % self.pname

    def __str__(self):
        return self.pname.encode('utf8')

    class Meta:
        verbose_name_plural = 'programmes'

class proggroups(models.Model):
    gname = models.CharField(max_length=10, help_text='Group name')
    programme = models.ForeignKey(programmes)

    def __str__(self):
        return self.gname


class locality(models.Model):
    townid = models.IntegerField()
    lname = models.CharField(max_length=255)

    def __str__(self):
        tmpstr = u'%s' % self.lname
        return tmpstr.encode('utf-8')

class people(models.Model):
    TOWN_CHOICES = (
            ('1', 'Adalar'),
            ('2', u'Bakırköy'),
            ('3', u'Beşiktaş'),
            ('4', 'Beykoz'),
            ('5', u'Beyoğlu'),
            ('6', u'Çatalca'),
            ('7', u'Eyüp'),
            ('8', 'Fatih'),
            ('9', u'Gaziosmanpaşa'),
            ('10', u'Kadıköy'),
            ('11', 'Kartal'),
            ('12', u'Sarıyer'),
            ('13', 'Silivri'),
            ('14', u'Şile'),
            ('15', u'Şişli'),
            ('16', u'Üsküdar'),
            ('17', 'Zeytinburnu'),
            ('18', u'Büyükçekmece'),
            ('19', u'Kağıthane'),
            ('20', u'Küçükçekmece'),
            ('21', 'Pendik'),
            ('22', u'Ümraniye'),
            ('23', 'Bayrampaşa'),
            ('24', u'Avcılar'),
            ('25', u'Bağcılar'),
            ('26', u'Bahçelievler'),
            ('27', u'Güngören'),
            ('28', 'Maltepe'),
            ('29', 'Sultanbeyli'),
            ('30', 'Tuzla'),
            ('31', 'Esenler'),
            ('32', u'Arnavutköy'),
            ('33', u'Ataşehir'),
            ('34', u'Başakşehir'),
            ('35', u'Beylikdüzü'),
            ('36', 'Çekmeköy'),
            ('37', 'Esenyurt'),
            ('38', 'Sancaktepe'),
            ('39', 'Sultangazi'),
    )
    PERSON_STATUS = (
            ('A', 'Aktif'),
            ('D', 'Deaktif(Arşiv)'),
            ('W', 'Beklemede'),
            ('H', 'Askıda'),
    )
    GENDER_CHOICES = (
            ('M', 'Male'),
            ('F', 'Female'),
    )
    MARITIAL_CHOICES = (
            ('M', 'Evli'),
            ('U', 'Bekar'),
            ('W', 'Dul'),
    )
    EDU_CHOICES = (
            ('P', 'İlkokul'),
            ('S', 'Ortaokul'),
            ('H', 'Lise'),
            ('U', 'Üniversite'),
            ('M', 'Master'),
            ('D', 'PhD'),
    )

    fname = models.CharField(max_length=255)
    sname = models.CharField(max_length=255)
    country = models.ForeignKey(country)
    children = models.ManyToManyField('self', blank=True, symmetrical=False)
    phonenum = models.CharField(max_length=20, null=True)
    idnum = models.CharField(max_length=100, null=True)
    notes = models.TextField(null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    birthdate = models.DateField(help_text='Date of birth MM/DD/YYYY')
    programmes = models.ManyToManyField(programmes, blank=True, symmetrical=False)
    archived = models.CharField(max_length=1, choices=PERSON_STATUS, default='A')
    regdate = models.DateField()
    updated = models.DateTimeField(auto_now=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default='F')
    maritialstatus = models.CharField(max_length=1, choices=MARITIAL_CHOICES, default='M')
    olddata = models.TextField(null=True, blank=True)
    deactivedate = models.DateTimeField(null=True, blank=True)
    arrivaldate = models.DateField(null=True, blank=True)
    education = models.CharField(max_length=1, choices=EDU_CHOICES, default='P')
    docscomplete = models.BooleanField(default=False)
    speaktr = models.BooleanField(default=False)
    town = models.IntegerField(choices=TOWN_CHOICES, blank=True, null=True)
    locality = models.ForeignKey(locality, on_delete=models.SET_NULL, null=True, blank=True)

    def age(self):
        '''Calculate the person's age in years.'''
        end = date.today()
        years = end.year - self.birthdate.year
        if end.month and self.birthdate.month:
            if end.month < self.birthdate.month or (end.month == self.birthdate.month and end.day and self.birthdate.day and end.day < self.birthdate.day):
                years -= 1
        return years

    def fullname(self):
        return u'%s %s' % (self.fname, self.sname)

    def get_absolute_url(self):
        return u"/people/?q=%s" % (self.fullname())

    class Meta:
        verbose_name_plural = 'people'

    def __unicode__(self):
        return unicode(self.fullname())

    def __str__(self):
        return self.fullname()

    objects = models.Manager()
    adult_objects = AdultManager()
    child_objects = ChildManager()
    agefilter = AgeLimitManager()

class doctype(models.Model):
    dtname = models.CharField(max_length=255)

    def __str__(self):
        return self.dtname

class document(models.Model):
    filename = models.FileField(upload_to=get_upload_path)
    doctype = models.ForeignKey(doctype)
    owner = models.ForeignKey(people)
    uploaddate = models.DateTimeField(auto_now_add=True, blank=True)
    description = models.CharField(max_length=255, null=True)

    def get_filename(self):
        return os.path.basename(self.filename.name)

    def __str__(self):
        return '%s - %s - %s' % (self.filename, self.doctype, self.owner.fullname())

class log(models.Model):
    user = models.ForeignKey(User)
    event = models.CharField(max_length=255)
    eventtime = models.DateTimeField(auto_now_add=True, blank=True)
    ipaddr = models.GenericIPAddressField(unpack_ipv4=True)

class attendance(models.Model):
    people = JSONField(default='{}')
    programme = models.ForeignKey(programmes)
    attdate = models.DateField(auto_now_add=True, blank=True)

    class Meta:
        unique_together = ('programme', 'attdate')


class orgdoc(models.Model):
    filename = models.FileField(upload_to="static/orgdocs/")
    description = models.CharField(max_length=255, null=True)
    uploaddate = models.DateTimeField(auto_now_add=True, blank=True)

    def get_filename(self):
        return os.path.basename(self.filename.name)

    def __str__(self):
        return '%s - %s' % (self.filename, self.description)
