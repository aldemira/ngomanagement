from django.utils import timezone
from haystack import indexes
from ngoman.models import people, programmes

class ProgIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    pname = indexes.CharField(model_attr='pname')

    def get_model(self):
        return programmes

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class PeopleIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    #fname = indexes.CharField(model_attr='fname')
    #sname = indexes.CharField(model_attr='sname')
    #idnum = indexes.CharField(model_attr='idnum')
    #updated = indexes.DateTimeField(model_attr='updated')
    fullname = indexes.CharField(model_attr='fullname')

    def get_model(self):
        return people

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
